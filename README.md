<div align="center">
   
   ## 🏗 Em Construção 🏗

   <h1>
      <a href="">
         <img src="">
      </a>
   </h1>

   O HerbarIA se trata de uma IA que realiza a detecção de espécies taxonômicas pertencentes a família das Fabaceaes, ele faz isso utilizando um modelo pré-treinado de análise de imagem.
</div>

## 💻 Team
- [Caio Souza Lima - Scrum master ](https://gitlab.com/caiosouzalima2002)
- [Bryan Maurício de Souza Francisco - Product owner ](https://gitlab.com/hellstrike12)
- [César Augusto Matos Ladeira - Developer](https://gitlab.com/ladeiraA)
- [João Pedro Aguilar Vicari - Developer](https://gitlab.com/GitJooJ)
- [Lucas Eduardo Dessy - Developer](https://gitlab.com/lucasdessy)

## ✨ Funcionalidades
- Análise de imagens de plantas
- Identifica o tipo fabaceae presente na imagem

## 🔨 Tecnologias

As seguintes ferramentas foram usadas:

- [Python](https://www.python.org/)
- [FastAPI](https://fastapi.tiangolo.com/)
- [TensorFlow](https://www.tensorflow.org/?hl=pt-br)
- [Flutter](https://flutter.dev/)

## 📦 Montagem do Ambiente do Desenvolvimento:

#### Instalando Python:

```sh
Baixar Instalador no site https://www.python.org/downloads/

```
#### FastAPI (Biblioteca do Python):

```sh
$ pip install fastapi[all]

```

#### TensorFlow (Biblioteca do Python):

```sh
$ pip install tensorflow

```
#### Instalando Flutter:

```sh
Baixar Instalador no site https://docs.flutter.dev/get-started/install

```
